public with sharing class SendEventEmailNotification {

    public static void sendMail(List<Event> eventObjList){
        List<String> strManagerEmailList = new List<String>();
        set<Id> ownerId = new set<id>();
        for(Event eventObj :eventObjList){
            ownerId.add(eventObj.OwnerId);
        }
        List<User> assignedUser=[select id,Name,ManagerId,Manager.Email,Manager.Name,Email from User where id IN :ownerId];
        Map<String,User> ownerInfoMap = new Map<String,User>();
        for(User userObj:assignedUser){
            if(!ownerInfoMap.containskey(userObj.id)){
                ownerInfoMap.put(userObj.id,userObj);
            }
        }
        for(Event eventObj :eventObjList){
            if(eventObj.Accompanied_by_Sales_Manager__c){
                String ownerManagerName;
                String ownerManagerEmail;
                if(ownerInfoMap.get(eventObj.OwnerId).ManagerId!=null){
                    ownerManagerName =ownerInfoMap.get(eventObj.OwnerId).Manager.Name;
                    ownerManagerEmail=ownerInfoMap.get(eventObj.OwnerId).Manager.Email;
                }else{
                    ownerManagerName =ownerInfoMap.get(eventObj.OwnerId).Name;
                    ownerManagerEmail=ownerInfoMap.get(eventObj.OwnerId).Email;
                }
                String bodyStr='Dear &nbsp;' +ownerManagerName+',<br /> <br/>Please note that '+ ownerInfoMap.get(eventObj.OwnerId).Name +' has created the following Event subject marked for Accompanied by Sales Manager : '+
                '<br /><a href='+Label.Event_link+'/'+eventObj.id+'>'+ eventObj.Subject+'</a><br /><br />Regards,<br/>'+ownerInfoMap.get(eventObj.OwnerId).Name +'<br /><br />';
                strManagerEmailList.add(ownerManagerEmail);
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.setToAddresses(strManagerEmailList);
                mail.setReplyTo(ownerInfoMap.get(eventObj.OwnerId).Email);
                mail.saveAsActivity = false;
                mail.setHtmlBody(bodyStr);
                mail.setSubject(Label.Event_Email_Subject);
                mail.setSenderDisplayName(ownerInfoMap.get(eventObj.OwnerId).Email);
                try{
                    Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
                }catch(EmailException ex){
                   eventObj.addError(ex);
                }
            }
        }
    }
}