@isTest
private class TestgetExternalRESTbasedWS {

    @isTest static void testhttpCallout() {
        // Set mock callout class
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        getexternamrestbasedws1 conr = new getexternamrestbasedws1();
        conr.responseFromWS();
            system.assertEquals('<env:Envelope><env:Header></env:Header><env:Body><ghr:return><ghr:array><ghr:item><ghr:account>PT015AC</ghr:account><ghr:key>PT015AC</ghr:key><ghr:lmMon>201205</ghr:lmMon><ghr:dspMon>May 2012</ghr:dspMon><ghr:total>0.00</ghr:total><ghr:totTrade>0.00</ghr:totTrade><ghr:status/></ghr:item></ghr:array></ghr:return></env:Body></env:Envelope>',conr.responseBody);
            }
}