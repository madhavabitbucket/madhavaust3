/*
Class Name : AsynchCreateQuoteHelper
Description  : 1. This helper class is used to extend the fuctionality of CreateQuoteExtension Class, In order to provide save fuctinality, 
                  Mass Update fuctionality,Zeroing of surcharges, Validation of records, and getting items from custom setting.
Created By    : Cognizant Technology Solutions
Created On   : 30/01/2012
Change Log
--------------------------------------------------------------------------
Sr# Description                                Updated By       Updated On
1   Added class comments                       CTS              30/01/2012
--------------------------------------------------------------------------  
*/ 

public class AsynchCreateQuoteHelper
{

    /*Global Variables for Error Messages*/
    public static boolean IsError;
    public static List<id> MEPCRouteIDs {get; set;}
    public static List<Rate__c> lstDuplicateportpair {get; set;}
    //Generate Mandatory string for the error message
    public static String GenerateMandatoryFieldsErrorString(Integer selectedReceiptCount,Integer selectedDeliveryCount,Integer selectedContainerSizeTypeCount){
        String localErrorString='Please select '; 
        List<String> localeMandatoryFieldList = new List<String>();
        
        try{
        
       
            if(selectedReceiptCount !=0 || selectedReceiptCount != null) localeMandatoryFieldList.add('Receipt Location');
            if(selectedDeliveryCount !=0 || selectedDeliveryCount !=null) localeMandatoryFieldList.add('Delivery Location');
            if(selectedContainerSizeTypeCount !=0 || selectedContainerSizeTypeCount !=null) localeMandatoryFieldList.add('Container Size Type');
            
            for(Integer count=0; count<localeMandatoryFieldList.size();count++){ 
                localErrorString += localeMandatoryFieldList.get(count);
                if(count != localeMandatoryFieldList.size()-1) localErrorString += ', ';
                if(count != localeMandatoryFieldList.size()-1) localErrorString += '.';
            }
        }catch(Exception ex){
            //Apexpages.Message('An unknown error occurred.Please contact System Administrator.');      
        }
         System.Debug('------['+localErrorString);
        return localErrorString;
    }
    
    //Remove selectOptions from the final SelectOption List  
    public Static List<Selectoption> RemoveSelectedLocations(List<Selectoption> selectedOptionList, List<String> removeselectedOptionList){
    for(String locationtoremove : removeselectedOptionList){
        for(Integer count=0;count<selectedOptionList.size();count++){
          if(locationtoremove !=selectedOptionList[count].getValue()){
            selectedOptionList.remove(count);
            break;
            }  
        }   
    }
    return selectedOptionList; 
    }
   
    
  
    
    // create commodity list from custom setting
    public static List<Commodity__c> CreateCommodityList(){
    List<Commodity__c> commodityList = new List<Commodity__c>();
        for(Commodity__c commodityObj : Commodity__c.getAll().values()){
        commodityList.add(commodityObj);
        }
    return commodityList;    
    }
    
  
  
   
    //wrapper class defination 
    public class ratewrapper
    { 
        public String rateUniqueId {get;set;}
        public Rate__c rate {get;set;}
        public List<Surcharge_Line_Item__c> surchargelineitemList {get;set;}
        public boolean showsurchargesflag {get;set;}
        public boolean rateSelected {get;set;}
        public boolean reefRateSelected {get;set;}
        public boolean showWarningFlag {get;set;} //For displaying warning image to RatelineItem if their surcharge type is blank
        
        //wrapper class constructor
        public ratewrapper(Rate__c rate, List<Surcharge_Line_Item__c> surchargelineitemList, Boolean showsurflag, Boolean isRateSelected)
        { 
            rateUniqueId = rate.id;
            this.rate = rate;
            this.surchargelineitemList = surchargelineitemList;
            showsurchargesflag = showsurflag;
            this.rateSelected =  isRateSelected;
            this.reefRateSelected = false;
        }     
    }
    
     
     
     
     
    //Added  by Tejas Kardile 22 Aug 2012
    //Created by Tejas - 2Aug2012
    //public Static SET<String> CreateMEPCRoute(Quote quoteObj, List<String> selectedReceiptLocationList, List<String> selectedDeliveryLocationList, Map<ID,geography__c> products, List<String> containerSizeType,String Cargo_Type, Boolean Haz, Set<String> commoditylist)
   
  
}