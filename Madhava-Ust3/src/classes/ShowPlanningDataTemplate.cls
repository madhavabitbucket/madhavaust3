public class ShowPlanningDataTemplate {

    public ApexPages.StandardSetController templates{get; set;}  
    public Boolean NoData{get;set;}       
    
    public ShowPlanningDataTemplate(ApexPages.StandardController controller) {       

        planningdatatemplates = New List<Quote_Template__c>();
        
        NoData = False;

        String type = ApexPages.currentPage().getParameters().get('Type');
        templates = new ApexPages.StandardSetController(Database.getQueryLocator([select Id, name, Type__c, Body__c, Tip__c, Owner.Name, Show_Tip__c from Quote_Template__c where Type__c = :type order by createddate desc ]));
   
        // sets the number of records in each page set  
        templates.setPageSize(2);          

        planningdatatemplates = templates.getRecords();
        
        if( planningdatatemplates.size() == 0 )
            NODATA = True;

    }
    
    public List<Quote_Template__c> planningdatatemplates
    {  
        get  
        {  
            if(templates != null)  
                return (List<Quote_Template__c>)templates.getRecords();  
            else
                return null ; 
        }  
        set;
    }  
/*        
    //Boolean to check if there are more records after the present displaying records
    public Boolean hasNext
    {
        get
        {
            return templates.getHasNext();
        }
        set;
    }
 
    //Boolean to check if there are more records before the present displaying records
    public Boolean hasPrevious
    {
        get
        {
            return templates.getHasPrevious();
        }
        set;
    }
 
    //Page number of the current displaying records
    public Integer pageNumber
    {
        get
        {
            return templates.getPageNumber();
        }
        set;
    }

    //Returns the previous page of records
    public void previous()
    {
        templates.previous();
    }
 
    //Returns the next page of records
    public void next()
    {
        templates.next();
    }
    
    public void first() 
    {
        templates.first();
    }

    //Returns the last page of records
    public void last() 
    {
        templates.last();
    }
    
    //Returns the number of Records in Set
    public Integer RecordsSize {
        get { return templates.getResultSize(); }
        set;
    }
*/    
}